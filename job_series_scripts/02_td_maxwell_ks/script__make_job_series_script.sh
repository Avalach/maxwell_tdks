#/bin/bash

IFS=''
rm job_series.sh

current_path_dir=`pwd`
cluster=$(echo $current_path_dir | awk -F/ '{print $6}')

while read body_line 
do

  if [[ $body_line == *job_series_variables* ]]
  then
    while read variables_line
    do
      echo $variables_line >> job_series.sh
    done < input_variables.$cluster
  elif [[ $body_line == *job_series_input* ]]
  then 
    while read input_line
    do
      echo $input_line >> job_series.sh
    done < job_series_input
  else
    echo $body_line >> job_series.sh
  fi

done < job_series_body

chmod 777 job_series.sh
