#!/bin/bash


### Cluster system variables and directory names  ##################################################

current_path_dir=`pwd`
cluster=$(echo $current_path_dir | awk -F/ '{print $6}')
# cluster="eos"         # chose cluster "eos", "hydra" or "draco"

### selecet cluster 
if [ "$cluster" == "eos" ]
  then
  dir_base="/scratch/rej"
  tasks_per_node=32
  run_time="240000"
elif [ "$cluster" == "hydra" ]
  then
  dir_base="/ptmp/rej"
  tasks_per_node=16
  run_time="24:00:00"
elif [ "$cluster" == "draco" ]
  then
  dir_base="/ptmp/rej"
  tasks_per_node=32
  run_time="24:00:00"
fi


### constants
c=137.035999679
pi=3.14159265359


### directory name list
octopus_maxwell_code_dir_base="/u/rej/octopus_maxwell/codes"
working_dir_path=`pwd`
working_dir_path_length=$(echo $working_dir_path | awk -F/ '{print NF}')
for kk in $( seq 1 $working_dir_path_length)
do
  working_dir_path_array[$kk]=$( echo $working_dir_path | cut -d'/' -f$kk)
done
octopus_exec=$octopus_maxwell_code_dir_base"/"${working_dir_path_array[7]}"/bin/octopus"
octopus_species_folder_psf=$octopus_maxwell_code_dir_base"/"${working_dir_path_array[7]}"/share/pseudopotentials/PSF/"
octopus_species_folder_hgh=$octopus_maxwell_code_dir_base"/"${working_dir_path_array[7]}"/share/pseudopotentials/HGH/"
echo ""
echo ""
echo "Executable file     : " $octopus_exec
echo ""
echo ""


### Job input variables and creating input files ###################################################


job_series_variables


### variable arrays for loop

cluster_select_array=($cluster_select_list)

calc_mode_array=($calc_mode_list)

lsize_x_array=($lsize_x_list)
lsize_y_array=($lsize_y_list)
lsize_z_array=($lsize_z_list)
spacing_x_array=($spacing_x_list)
spacing_y_array=($spacing_y_list)
spacing_z_array=($spacing_z_list)

mx_lsize_x_array=($mx_lsize_x_list)
mx_lsize_y_array=($mx_lsize_y_list)
mx_lsize_z_array=($mx_lsize_z_list)
mx_spacing_x_array=($mx_spacing_x_list)
mx_spacing_y_array=($mx_spacing_y_list)
mx_spacing_z_array=($mx_spacing_z_list)

extra_states_array=($extra_states_list)

exp_order_array=($exp_order_list)
der_order_array=($der_order_list)

ab_width_array=($ab_width_list)
output_interval_array=($output_interval_list)

dt_array=($dt_list)
max_steps_array=($max_steps_list)
max_time_array=($max_time_list)
mx_interval_array=($mx_interval_list)

cfl_array=($cfl_list)

mx_etrs_approx_array=($mx_etrs_approx_list)

mx_ks_mode_array=($mx_ks_mode_list)

td_mode_array=($td_mode_list)

pulse_type_array=($pulse_type_list)
amplitude_array=($amplitude_list)
lambda_array=($lambda_list)
pulse_width_array=($pulse_width_list)
pulse_shift_array=($pulse_shift_list)
wave_type_array=($wave_type_list)
envelope_array=($envelope_list)

j_max_array=($j_max_list)
t_width_array=($t_width_list)
t_shift_array=($t_shift_list)

eigensolver_array=($eigensolver_list)
err_tol_array=($err_tol_list)
conv_energy_array=($conv_energy_list)
conv_rel_dens_array=($conv_rel_dens_list)

core_number_array=($core_number_list)

loop_array_number=${#core_number_array[@]}

### job loops
idx=0
for ii in $( seq 1 $loop_array_number)
do

  echo ""
  echo "Job Nr.:  $ii"
  echo "" 

  idx=$( echo " $ii-1 " | bc )

  cluster_select=${cluster_select_array[$idx]}


### input variables ################################################################################

  ### job name
  job_name="\${system_name}_${calc_mode_array[$idx]}"

  ### core and node variables
  number_cores=${core_number_array[$idx]}
  number_nodes=$( echo "$number_cores/$tasks_per_node" | bc )
  number_cores=$( printf '%04d' $number_cores )

  ### calculation mode
  calculation_mode=${calc_mode_array[$idx]}

  ### eigensolver variables
  eigensolver=${eigensolver_array[$idx]}
 # eigensolver="rmmdiis"
  error_tolerance=${err_tol_array[$idx]}
  error_tolerance="$( printf '%020.18f' $error_tolerance )"
  error_tolerance="$( printf '%4.2e' "$( echo " $error_tolerance " | bc -l)" )"

  ### convergence criterion
  conv_energy=${conv_energy_array[$idx]}
  conv_energy="$( printf '%020.18f' $conv_energy )"
  conv_energy="$( printf '%4.2e' "$( echo " $conv_energy " | bc -l)" )"
  conv_rel_dens=${conv_rel_dens_array[$idx]}
  conv_rel_dens="$( printf '%020.18f' $conv_rel_dens )"
  conv_rel_dens="$( printf '%4.2e' "$( echo " $conv_rel_dens " | bc -l)" )"

  ### matter system size variables
  lsize_x=${lsize_x_array[$idx]}
  lsize_y=${lsize_y_array[$idx]}
  lsize_z=${lsize_z_array[$idx]}
  spacing_x=${spacing_x_array[$idx]}
  spacing_y=${spacing_y_array[$idx]}
  spacing_z=${spacing_z_array[$idx]}
  lsize_x="$(  printf '%010.3f' "$( echo " $lsize_x   " | bc -l)" )"
  lsize_y="$(  printf '%010.3f' "$( echo " $lsize_y   " | bc -l)" )"
  lsize_z="$(  printf '%010.3f' "$( echo " $lsize_z   " | bc -l)" )"
  spacing_x="$(printf '%010.3f' "$( echo " $spacing_x " | bc -l)" )"
  spacing_y="$(printf '%010.3f' "$( echo " $spacing_y " | bc -l)" )"
  spacing_z="$(printf '%010.3f' "$( echo " $spacing_z " | bc -l)" )"

  ### maxwell system size variables
  mx_lsize_x=${mx_lsize_x_array[$idx]}
  mx_lsize_y=${mx_lsize_y_array[$idx]}
  mx_lsize_z=${mx_lsize_z_array[$idx]}
  mx_spacing_x=${mx_spacing_x_array[$idx]}
  mx_spacing_y=${mx_spacing_y_array[$idx]}
  mx_spacing_z=${mx_spacing_z_array[$idx]}
  mx_lsize_x="$(  printf '%010.3f' "$( echo " $mx_lsize_x   " | bc -l)" )"
  mx_lsize_y="$(  printf '%010.3f' "$( echo " $mx_lsize_y   " | bc -l)" )"
  mx_lsize_z="$(  printf '%010.3f' "$( echo " $mx_lsize_z   " | bc -l)" )"
  mx_spacing_x="$(printf '%010.3f' "$( echo " $mx_spacing_x " | bc -l)" )"
  mx_spacing_y="$(printf '%010.3f' "$( echo " $mx_spacing_y " | bc -l)" )"
  mx_spacing_z="$(printf '%010.3f' "$( echo " $mx_spacing_z " | bc -l)" )"

  ### geometry file
  geometry_file[1]=$system_name".geometry_z_dir.xyz"
  geometry_path[1]=$working_dir_path

  ### species files
  species[1]="H"
  species_file_psf[1]="H.psf"
  species_file_hgh[1]="H.hgh"
  species_path_psf[1]=$octopus_species_folder_psf
  species_path_hgh[1]=$octopus_species_folder_hgh
  species[2]="C"
  species_file_psf[2]="C.psf"
  species_file_hgh[2]="C.hgh"
  species_path_psf[2]=$octopus_species_folder_psf
  species_path_hgh[2]=$octopus_species_folder_hgh
  species[3]="Na"
  species_file_psf[3]="Na.psf"
  species_file_hgh[3]="Na.hgh"
  species_path_psf[3]=$octopus_species_folder_psf
  species_path_hgh[3]=$octopus_species_folder_hgh
  species[4]="Cl"
  species_file_hgh[4]="Cl.hgh"
  species_path_hgh[4]=$octopus_species_folder_hgh

  ### extra states 
  extra_states="$( printf '%02d' "${extra_states_array[$idx]}" )"

  ### operator variables
  exp_order=${exp_order_array[$idx]}
  der_order=${der_order_array[$idx]}

  ### Output
  output_interval=${output_interval_array[$idx]}

  ### absorbing boundaries variables
  ab_width=${ab_width_array[$idx]}

  ### courant Maxwell dt
  courant_dt=$( echo " ${cfl_array[$idx]} / ( sqrt($c^2/$mx_spacing_x^2 + $c^2/$mx_spacing_y^2 + $c^2/$mx_spacing_z^2) ) " | bc -l)
  courant_dt="$(printf '%09.7f' "$( echo " $courant_dt " | bc -l)" )"

  ### Maxwell interval steps
  mx_interval_steps=${mx_interval_array[$idx]}
  mx_interval_steps_string=$( printf '%04d' $mx_interval_steps )

  ### dt
 # dt=${dt_array[$idx]}                                         # dt selected by input list above
 # dt=$courant_dt                                               # dt selected as courant time
  dt=$( echo " $mx_interval_steps * $courant_dt " | bc -l )    # dt corresponds to courant time times maxwell interval steps
  dt="$(printf '%09.7f' "$( echo " $dt " | bc -l)" )"

  ### mx_dt
  mx_dt=$( echo " $dt / $mx_interval_steps " | bc -l)
  mx_dt="$(printf '%09.7f' "$( echo " $mx_dt " | bc -l)" )"

  ### Maximum time steps
 # max_time_steps=${max_steps_array[$idx]}                            # max_time_steps selected by input list above
  max_time_steps=$( echo " ${max_time_array[$idx]} / $dt " | bc )    # max_time_steps selected by max time

  ### Maxwell etrs approximation
  mx_etrs_approx=${mx_etrs_approx_array[$idx]}

  ### maxwell ks mode
  maxwell_ks_mode=${mx_ks_mode_array[$idx]}

  ### electric field variables
  lambda=${lambda_array[$idx]}
  amplitude=${amplitude_array[$idx]}
  pulse_type=${pulse_type_array[$idx]}
  pulse_width=${pulse_width_array[$idx]}
  pulse_shift=${pulse_shift_array[$idx]}
  wave_type=${wave_type_array[$idx]}
  envelope=${envelope_array[$idx]}
  amplitude="$(   printf '%15.12f' "$( echo " $amplitude       " | bc -l)" )"
  lambda="$(      printf '%10.4f'  "$( echo " $lambda          " | bc -l)" )"
  omega="$(       printf '%15.12f' "$( echo " 2*$pi*$c/$lambda " | bc -l)" )"
  k="$(           printf '%15.12f' "$( echo " 2*$pi/$lambda    " | bc -l)" )"
  pulse_width="$( printf '%10.4f'  "$( echo " $pulse_width     " | bc -l)" )"
  pulse_shift="$( printf '%10.4f'  "$( echo " $pulse_shift     " | bc -l)" )"

  ### external current variables
  j_max=${j_max_array[$idx]}
  j_max="$(  printf '%015.12f' "$( echo " $j_max   " | bc -l)" )"
  t_shift=${t_shift_array[$idx]}
  t_shift="$(printf '%015.12f' "$( echo " $t_shift " | bc -l)" )"
  t_width=${t_width_array[$idx]}
  t_width="$(printf '%015.12f' "$( echo " $t_width " | bc -l)" )"
  current_omega=0.0

  ### td mode
  td_mode=${td_mode_array[$idx]}

  ### classical field variables
  td_mode="laser"
  lambda_classical=$lambda
  omega_classical=$omega
  amplitude_classical=$amplitude
  t0_classical=$( echo   " (-1)*$pulse_shift/$c " | bc -l)
  tau0_classical=$( echo "      $pulse_width/$c " | bc -l)

  if [[ $maxwell_ks_mode == external_field ]] && [[ $calculation_mode == maxwell_ks ]]
  then
    if [[ $envelope == const ]]
    then
      a0=''
    else
      a0='#'
    fi
    if [[ $envelope == gaussian ]]
    then
      a1=''
    else
      a1="#"
    fi
    if [[ $envelope == cosinoidal ]]
    then
      a2=''
    else
      a2="#"
    fi
  else
    a0='#'
    a1='#'
    a2='#'
  fi


### creating job folders ###########################################################################

  job_sub="no"

  ### strings for folders
  lsize_spacing_string=$( printf "boxsize_%5.3e_%5.3e_%5.3e__spacing_%5.3e_%5.3e_%5.3e" "$lsize_x" "$lsize_y" "$lsize_z" "$spacing_x" "$spacing_y" "$spacing_z" )
  # lsize_spacing_string="boxsize_"$lsize_x"_"$lsize_y"_"$lsize_z"__spacing_"$spacing_x"_"$spacing_y"_"$spacing_z
  mx_lsize_mx_spacing_string=$( printf "mx_boxsize_%5.3e_%5.3e_%5.3e__mx_spacing_%5.3e_%5.3e_%5.3e" "$mx_lsize_x" "$mx_lsize_y" "$mx_lsize_z" "$mx_spacing_x" "$mx_spacing_y" "$mx_spacing_z" )
  # mx_lsize_mx_spacing_string="mx_boxsize_"$mx_lsize_x"_"$mx_lsize_y"_"$mx_lsize_z"__mx_spacing_"$mx_spacing_x"_"$mx_spacing_y"_"$mx_spacing_z
  laser_gauss_pulse_string=$( printf "laser_%s__lambda_%8.6e__amplitude_%5.3e__pulse_width_%8.6e__pulse_shift_%8.6e" "$pulse_type" "$lambda" "$amplitude" "$pulse_width" "$pulse_shift" )
  # laser_gauss_pulse_string="laser_"$pulse_type"__lambda_"$lambda"__amplitude_"$amplitude"__pulse_width_"$pulse_width"__pulse_shift_"$pulse_shift
  external_current_string=$( printf "current_shape_%s__current_max_%5.3e__current_width_%8.6e__time_shift_%8.6e" "$current_shape" "$j_max" "$t_width" "$t_shift" )
  external_current_string="current_shape_"$current_shape"__current_max_"$j_max"__current_width_"$t_width"__time_shift_"$t_shift
  dt_string=$( printf "dt_%4.2e" "$dt")
  mx_dt_string=$( printf "mx_dt_%4.2e" "$mx_dt")

  ### groundstate dir name
  gs_dir="01_groundstate"
  gs_job_name=${lsize_spacing_string}"__extra_states_"$extra_states"__eigensolver_"$eigensolver"__error_tolerance_"$error_tolerance"__conv_energy_"$conv_energy"__conv_rel_dens_"$conv_rel_dens

  ### making job directories
  run_dir=$dir_base
  sys_dir=$dir_base
  for kk in $( seq 4 $working_dir_path_length)
  do
    run_dir=$run_dir/${working_dir_path_array[$kk]}
    run_dir_array[$kk]=${working_dir_path_array[$kk]}
    run_dir_number=$kk
  done
  for kk in $( seq 4 $working_dir_path_length)
  do
    sys_dir=$sys_dir/${working_dir_path_array[$kk]}
    if [[ ${working_dir_path_array[$kk]} == *$system_name* ]]
    then
      break
    fi
  done
  if [ "$calculation_mode" == "gs" ]
  then
    job_dir=${lsize_spacing_string}"__extra_states_"$extra_states"__eigensolver_"$eigensolver"__error_tolerance_"$error_tolerance"__conv_energy_"$conv_energy"__conv_rel_dens_"$conv_rel_dens
  elif [ "$calculation_mode" == "maxwell_ks" ]
  then
    if [ "$maxwell_ks_mode" == "no_external_field" ]
    then
      run_dir_array[$( echo "$run_dir_number+1" | bc)]=${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      run_dir=$run_dir/${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      job_dir=$dt_string"__"$mx_dt_string"__mx_interval_steps_"$mx_interval_steps_string"__cores_"$number_cores
    elif [ "$maxwell_ks_mode" == "external_field" ]
    then
      run_dir_array[$( echo "$run_dir_number+1 " | bc )]=${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      run_dir=$run_dir/${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      job_dir=$laser_gauss_pulse_string"__"$dt_string"__"$mx_dt_string"__mx_interval_steps_"$mx_interval_steps_string"__mx_etrs_approx_"$mx_etrs_approx"__cores_"$number_cores
    elif [ "$maxwell_ks_mode" == "external_current" ]
    then
      run_dir_array[$( echo "$run_dir_number+1" | bc )]=${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      run_dir=$run_dir/${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      job_dir=$dt_string"__"$mx_dt_string"__"$external_current_string"__cores_"$number_cores
    fi
  elif [ "$calculation_mode" == "td" ]
  then
    if [ "$td_mode" == "laser" ]
    then
      run_dir_array[$( echo "$run_dir_number+1" | bc )]=${lsize_spacing_string}
      run_dir=$run_dir/${lsize_spacing_string}
      job_dir=$laser_gauss_pulse_string"__"$dt_string"__cores_"$number_cores
    else
      run_dir_array[$( echo "$run_dir_number+1" | bc )]=${lsize_spacing_string}
      run_dir=$run_dir/${lsize_spacing_string}
      job_dir=$dt_string"__cores_"$number_cores
    fi
  elif [ "$calculation_mode" == "maxwell_free" ]
  then
    if [ "$maxwell_ks_mode" == "external_current" ]
    then
      run_dir_array[$( echo "$run_dir_number+1" | bc)]=${mx_lsize_mx_spacing_string}
      run_dir=$run_dir/${mx_lsize_mx_spacing_string}
      job_dir=$dt_string"__"$mx_dt_string"__"$external_current_string"__cores_"$number_cores
    elif [ "$maxwell_ks_mode" == "external_field" ]
    then
      run_dir_array[$( echo "$run_dir_number+1 " | bc )]=${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      run_dir=$run_dir/${lsize_spacing_string}"__"${mx_lsize_mx_spacing_string}
      job_dir=$laser_gauss_pulse_string"__"$dt_string"__"$mx_dt_string"__mx_interval_steps_"$mx_interval_steps_string"__mx_etrs_approx_"$mx_etrs_approx"__cores_"$number_cores
    fi
  fi 

  ### check if job shall run on this cluster
  if [ $cluster_select == "off" ]
  then
    job_sub="off"
  elif [ $cluster_select == $cluster ] || [ $cluster_select == "all" ]
  then
    job_sub="yes"
  elif [ $cluster_select == "small" ] && ( [ $cluster == "eos" ] || [ $cluster == "draco" ] )
  then
    job_sub="yes"
  elif [ $cluster_select != $cluster ]
  then
    job_sub="wrong_cluster"
  elif [ $cluster_select != $cluster ] && [ $cluster_select != "all" ]
  then
    if [ $cluster_select == "small" ] && ( [ $cluster == "hydra" ] )
    then
      job_sub="wrong_cluster"
    fi
    if [ $cluster_select == "large" ] && ( [ $cluster == "eos" ] || [ $cluster == "draco" ] )
    then
      job_sub="wrong_cluster"
    fi
  else
    job_sub="error"
  fi

  ### making run derectories in "scratch"
  echo "Making requested run directories:"
  mk_dir=$dir_base
  for kk in $(seq 4 $( echo "3+${#run_dir_array[@]}" |bc ))
  do
    mk_dir=$mk_dir"/"${run_dir_array[$kk]}
    echo "make directory      : " $mk_dir
    if [ ! -d $mk_dir ] && [ $job_sub == "yes" ]
    then
      mkdir $mk_dir
    fi
  done
  echo ""

  ### making complete job directory
  job_complete_dir=$run_dir"/"$job_dir
  if [ ! -d $job_complete_dir ] && [ $job_sub != "off" ] && [ $job_sub != "wrong_cluster" ]
  then
    if [ $job_sub == "yes" ]
    then
      echo "make job directory  : " $job_complete_dir
      mkdir $job_complete_dir
    fi
  elif [ -d $job_complete_dir ]
  then
    job_sub="done"
  fi
  echo ""

  ### copy necessary folders
  if [ -d $job_complete_dir ] && [ $job_sub != "off" ]
  then
    echo "pushd command:"
    pushd $job_complete_dir
    echo ""

    ### copy species and geometry files 
    for kk in $( seq 1 ${#geometry_file[@]} )
    do
      if [ -e ${geometry_path[$kk]}/${geometry_file[$kk]} ]
      then
        cp ${geometry_path[$kk]}/${geometry_file[$kk]} .
      fi
    done
    for kk in $( seq 1 ${#species_file_psf[@]} )
    do
      if [ -e ${species_path_psf[$kk]}/${species_file_psf[$kk]} ]
      then
        cp ${species_path_psf[$kk]}/${species_file_psf[$kk]} .
      fi
    done
    for kk in $( seq 1 ${#species_file_hgh[@]} )
    do
      if [ -e ${species_path_hgh[$kk]}/${species_file_hgh[$kk]} ]
      then
        cp ${species_path_hgh[$kk]}/${species_file_hgh[$kk]} .
      fi
    done

    ### copy groundstate restart folder
    if [ "$calculation_mode" == "maxwell_ks" ] || [ "$calculation_mode" == "td" ]
    then
      if [ ! -d restart ]
      then
        echo "make restart directory"
        mkdir restart
      fi
      groundstate_folder="$sys_dir/$gs_dir/$gs_job_name/restart/gs"
      if [ -e $groundstate_folder ]
      then
        if [ ! -d ./restart/gs ] && [ $job_sub != "off" ]
        then
          echo "Copy groundtate folder from groundstate calculation into restart folder."
          echo ""
          cp -r $groundstate_folder ./restart/
        else
          echo "Groundstate folder is already copied into restart folder."
          echo ""
        fi
      else
        echo "Could not find necessary groundstate folder!"
        echo ""
      fi
      if [ ! -d ./restart/gs ]
      then
        job_sub="no"
      fi
    fi


### making input file ##############################################################################

cat <<- EOF > inp

job_series_input

EOF


### job scripts ####################################################################################

### create job script for eos ######################################################################

    if [ "$cluster" == "eos" ]
      then
  
cat <<- EOF > submit_mpi.sge

#######################################################
#example SGE batch script for a plain MPI program using
#512 cores (16 nodes x 32 cores per node) with 32 MPI 
#tasks per node
#executed under the intel runtime 
#######################################################

## run in /bin/bash
#$ -S /bin/bash
## name of the output file
#$ -o ./octopus.\\$JOB_ID.out
## name of the error file
#$ -e ./octopus.\\$JOB_ID.err
## do not join stdout and stderr
#$ -j n
## name of the job
#$ -N $job_name
## execute job from the current working directory
#$ -cwd
## send mail
#$ -m abe
#$ -M jestaedt@fhi-berlin.mpg.de
## request 16 $number_nodes (x 32 cores), must be a multiple of 32
#$ -pe impi_hydra_3d $number_cores
## run for 30 minutes
#$ -l h_rt=$run_time

module load intel impi

## to save memory at high core counts, the "connectionless user datagram protocol" 
## can be enabled (might come at the expense of speed)
## see https://software.intel.com/en-us/articles/dapl-ud-support-in-intel-mpi-library
# export I_MPI_DAPL_UD=1

##gather MPI statistics to be analyzed with itac's mps tool
# export I_MPI_STATS=all

##gather MPI debug information (high verbosity)
# export I_MPI_DEBUG=5

## qsub -hold_jid \\$JOB_ID submit_mpi.sge

mpiexec $octopus_exec

EOF

      ### submit job
      if [ "$job_sub" == "yes" ]
      then
        qsub submit_mpi.sge
      elif [ "$job_sub" == "done" ]
      then
        echo "Job Nr. $ii was already submitted!"
      else
        echo "Job Nr. $ii was not submitted!"
      fi


### create job script for hydra ####################################################################

    elif [ "$cluster" == "hydra" ]
      then

cat <<- EOF > submit_mpi.ll
# @ shell=/bin/bash
#
# Sample script for LoadLeveler
#
## @ step_name = \\$(jobid)
# @ error = octopus.\\$(jobid).err
# @ output = octopus.\\$(jobid).out
# @ job_name = $job_name
# @ job_type = parallel
# @ node_usage= not_shared
# @ node =$number_nodes
# @ tasks_per_node = $tasks_per_node
# @ resources = ConsumableCpus(1)
# @ network.MPI = sn_all,not_shared,us
# @ wall_clock_limit = $run_time
# @ notification = always
# @ notify_user = jestaedt@fhi-berlin.mpg.de
# @ queue

# run the program

poe $octopus_exec

##@ dependency = (\\$(jobid) >= 0)
## @ error = submit_mpi.\\$(jobid).err
## @ output = submit_mpi.\\$(jobid).out
## @ job_name = $job_name
## @ job_type = parallel
## @ node_usage= not_shared
## @ node =$number_nodes
## @ tasks_per_node = $tasks_per_node
## @ resources = ConsumableCpus(1)
## @ network.MPI = sn_all,not_shared,us
## @ wall_clock_limit = $run_time
## @ notification = always
## @ notify_user = jestaedt@fhi-berlin.mpg.de
## @ executable = submit_mpi.ll
## @ queue

EOF

      ### submit job
      if [ "$job_sub" == "yes" ]
      then
        llsubmit submit_mpi.ll
      elif [ "$job_sub" == "done" ]
      then
        echo "Job Nr. $ii was already submitted!"
      else
        echo "Job Nr. $ii was not submitted!"
      fi


### create job script draco ########################################################################

    elif [ "$cluster" == "draco" ]
      then

cat <<- EOF > submit_mpi.slurm
#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./octopus.%j.out
#SBATCH -e ./octopus.%j.err
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J $job_name
# Queue (Partition):
#SBATCH --partition=general
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=$number_nodes
#SBATCH --ntasks-per-node=$tasks_per_node
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jestaedt@fhi-berlin.mpg.de
#

# Request 120 GB of main Memory per node in Units of MB:
#SBATCH --mem=122000

#SBATCH --mail-type=none
#SBATCH --mail-user=\\$(user)@rzg.mpg.de
#
# Wall clock limit:
#SBATCH --time=$run_time

module load intel/16.0 mkl/11.3 impi/5.1.3 gsl/2.1 fftw/3.3.4

# Daisy chain for job submision
## sbatch -d afterany:$SLURM_JOB_ID submit_mpi.sbatch

# Run the program:
srun $octopus_exec

EOF

      ### submit job
      if [ "$job_sub" == "yes" ]
      then
        sbatch submit_mpi.slurm
      elif [ "$job_sub" == "done" ]
      then
        echo "Job Nr. $ii was already submitted!"
      else
        echo "Job Nr. $ii was not submitted!"
      fi

    fi

    echo ""
    if [ $job_sub != "off" ]
      then
      echo "popd command:"
      popd
    fi

  elif [ "$job_sub" == "wrong_cluster" ]
  then
    echo "Job Nr. $ii is submitted on the wrong cluster!"
  elif [ "$job_sub" == "off" ]
  then
    echo "Job Nr. $ii is manually not submitted!"
  else
    echo "Error! Job Nr. $ii could not be submitted!"
  fi

  echo ""
  echo ""

done


