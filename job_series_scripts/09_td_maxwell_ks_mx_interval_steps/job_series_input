
 Debug                             = trace

# ProfilingMode                     = yes

 CalculationMode                   = $calculation_mode

 ParDomains                        = auto
 ParStates                         = no
 MaxwellParDomains                 = auto
 MaxwellParStates                  = no


# ----- Matter box variables ----------------------------------------------------------------------

 Dimensions                        = 3
 BoxShape                          = PARALLELEPIPED

 %Lsize
  $lsize_x | $lsize_y | $lsize_z
 %

 %Spacing
  $spacing_x | $spacing_y | $spacing_z
 %


# ----- Maxwell box variables ---------------------------------------------------------------------

 MaxwellDimensions                 = 3
 MaxwellBoxShape                   = PARALLELEPIPED

 %MaxwellLsize
  $mx_lsize_x | $mx_lsize_y | $mx_lsize_z
 %
 
 %MaxwellSpacing
  $mx_spacing_x | $mx_spacing_x | $mx_spacing_x
 %


# ----- Species variables -------------------------------------------------------------------------

# %Coordinates   # example for Na2
#  '${species[3]}' | 0 | 0 | -1.53945
#  '${species[3]}' | 0 | 0 |  1.53945
# %

 %Species
 '${species[1]}'       | species_pseudo         | file | '${species_file_psf[1]}'
 '${species[2]}'       | species_pseudo         | file | '${species_file_psf[2]}'
 %

 XYZCoordinates                    = "${geometry_file[1]}"


# ----- Matter calculation variables --------------------------------------------------------------

 ExtraStates                       = $extra_states

# XCFunctional                     = gga_x_pbe_r + gga_c_pbe
 XCFunctional                      = lda_x + lda_c_gl

 EigenSolver                       = $eigensolver
 EigenSolverTolerance              = $error_tolerance
# ConvEigenError                    = yes
 EigensolverMaxIter                = 100
 ConvEnergy                        = $conv_energy
 ConvRelDens                       = $conv_rel_dens
 MaximumIter                       = -1

 MoveIons                          = yes

 TDExpOrder                        = $exp_order
 DerivativesStencil                = stencil_starplus
 DerivativesOrder                  = $der_order

 AbsorbingBoundaries               = mask
 ABWidth                           = $ab_width


# ----- Maxwell calculation variables -------------------------------------------------------------

 MaxwellHamiltonianOperator        = spin_matrix
 MaxwellTDOperatorMethod           = maxwell_op_fd
 MaxwellTDPropagator               = maxwell_etrs

 MatterToMaxwellCoupling           = yes
 MaxwellToMatterCoupling           = yes
 MaxwellCouplingOrder              = electric_dipole_coupling + magnetic_dipole_coupling + electric_quadrupole_coupling 

 MaxwellTDExpOrder                 = $exp_order
 MaxwellDerivativesStencil         = stencil_starplus
 MaxwellDerivativesOrder           = $der_order

 MaxwellFreePropagationTest        = no
 MaxwellTDEnergyCalculationIter    = 1

 MaxwellIncidentWavesEnergyCalc    = yes
 MaxwellIncidentWavesEnergyCalcIter= 10

 MaxwellABWidth                    = $ab_width
 MaxwellAbsorbingBoundaries        = mask

 MaxwellTDTransEfieldCalculation   = no
 MaxwellTDEfieldTransVariance      = no
 MaxwellTDEfieldTransVarianceIter  = 10
 MaxwellTDEfieldDivergence         = no
 MaxwellTDEfieldDivergenceIter     = 10


# ----- Output variables --------------------------------------------------------------------------

 OutputFormat                      = plane_x + plane_y + plane_z + vtk + xyz


# ----- Matter output variables -------------------------------------------------------------------

 Output                            = density + current + geometry + forces + elf
 OutputInterval                    = $output_interval
# TDOutput                          = energy + geometry + multipoles + laser


# ----- Maxwell output variables ------------------------------------------------------------------

 MaxwellOutput                     = electric_field + magnetic_field + maxwell_energy_density + poynting_vector
 MaxwellOutputInterval             = $output_interval
 MaxwellTDOutput                   = maxwell_energy + maxwell_fields


# ----- Time step variables -----------------------------------------------------------------------

 TDTimeStep                        = $dt
 TDMaxSteps                        = $max_time_steps
 TDEnergyUpdateIter                = 1
 MaxwellTDIntervalSteps            = $mx_interval_steps

 MaxwellTDETRSApprox               = $mx_etrs_approx


# ----- Maxwell field variables -------------------------------------------------------------------
 pi=3.14159265359
 c=137.035999679

# ----- laser propagates in x direction ---------
 lambda      = $lambda
 omega       = $omega
 kx          = $k
 Ez          = $amplitude
 pulse_width = $pulse_width
 pulse_shift = $pulse_shift

# %UserDefinedMaxwellIncidentWaves
#  incident_wave_parser | "kx" | "0"  | "0" | "0" | "0" | "Ez*cos(kx*(x-pulse_shift))*exp(-((x-pulse_shift)^2/(2*pulse_width^2)))" | $wave_type
# %

 %UserDefinedMaxwellIncidentWaves
   incident_wave_mx_function | 0 | 0 | Ez | "wave_function" | plane_wave
 %

 %MaxwellFunctions
  "wave_function" | mxf_gaussian_wave | kx | 0 | 0 | pulse_shift | 0 | 0 | pulse_width 
 %

  # const wave
$a0 %UserDefinedMaxwellStates
$a0  3 | formula | electric_field |        " Ez * cos( kx * ( x - pulse_shift ) ) "
$a0  2 | formula | magnetic_field | " -1/c * Ez * cos( kx * ( x - pulse_shift ) ) "
$a0 %

  # gaussian wave
$a1 %UserDefinedMaxwellStates
$a1  3 | formula | electric_field |        " Ez * cos( kx * ( x - pulse_shift ) ) * exp( -( ( x - pulse_shift )^2 / ( 2 * pulse_width^2 ) ) ) "
$a1  2 | formula | magnetic_field | " -1/c * Ez * cos( kx * ( x - pulse_shift ) ) * exp( -( ( x - pulse_shift )^2 / ( 2 * pulse_width^2 ) ) ) "
$a1 %

 # cosinoidal wave
$a2 %UserDefinedMaxwellStates
$a2  3 | formula | electric_field |        " Ez * cos( kx * ( x - pulse_shift ) ) * cos( pi/2 * ( x - 2 * pulse_width - pulse_shift ) / pulse_width ) * step( pulse_width - abs( ( kx*x - kx*pulse_shift ) / kx^2) ) "
$a2  2 | formula | magnetic_field | " -1/c * Ez * cos( kx * ( x - pulse_shift ) ) * cos( pi/2 * ( x - 2 * pulse_width - pulse_shift ) / pulse_width ) * step( pulse_width - abs( ( kx*x - kx*pulse_shift ) / kx^2) ) "
$a2 %


# ----- laser propagates in z direction ---------
# lambda      = $lambda
# omega       = $omega
# kz          = $k
# Ex          = $amplitude
# pulse_width = $pulse_width
# pulse_shift = $pulse_shift

# %UserDefinedMaxwellIncidentWaves
#  incident_wave_parser | "0" | "0" | "kz" | "Ex * cos(kz*(z-pulse_shift)) * exp(-((z-pulse_shift)/pulse_width)^2)" | "0" | "0" | $wave_type
# %

# %UserDefinedMaxwellIncidentWaves
#   incident_wave_mx_function | Ex | 0 | 0 | "wave_function" | plane_wave
# %

# %MaxwellFunctions
#  "wave_function" | mxf_gaussian_wave | 0 | 0 | kz | 0 | 0 | pulse_shift | pulse_width
# %

  # const wave
#$a0 %UserDefinedMaxwellStates
#$a0  1 | formula | "Ex*cos(kz*(z-pulse_shift))" | "0                             "
#$a0  2 | formula | "0                         " | "1/c*Ex*cos(kz*(z-pulse_shift))"
#$a0  3 | formula | "0                         " | "0                             "
#$a0 %

  # gaussian wave
#$a1 %UserDefinedMaxwellStates
#$a1  1 | formula | "Ex*cos(kz*(z-pulse_shift))*exp(-((z-pulse_shift)^2/(2*pulse_width^2)))" | "0                                                                       "
#$a1  2 | formula | "0                                                                     " | "1/c*Ex*cos(kz*(z-pulse_shift))*exp(-((z-pulse_shift)/(2*pulse_width^2)))"
#$a1  3 | formula | "0                                                                     " | "0                                                                       "
#$a1 %

  # cosinoidal wave
#$a2 %UserDefinedMaxwellStates
#$a2  1 | formula | "Ex*cos(kz*(z-pulse_shift))*cos(pi/2*(z-2*pulse_width-pulse_shift)/pulse_width)" | "0                                                                                 "
#$a2  2 | formula | "0                                                                             " | "1/c*Ex*cos(kz*(z-pulse_shift))*cos(pi/2*(z-2*pulse_width-pulse_shift)/pulse_width)"
#$a2  3 | formula | "0                                                                             " | "0                                                                                 "
#$a2 %


# ----- external current ------------------------
# MaxwellExternalCurrent            = no

# t0   = $t0
# js   = $js
# jmax = $jmax

# %UserDefinedMaxwellExternalCurrent
#    "0" | "0" | "jmax*step(x)*step(-x)*step(y)*step(-y)*exp(-((t-t0)/js)^2)"
# %


# ----- classical external field for td run -------------------------------------------------------
# lambda= $lambda_classical
# omega = $omega_classical
# Ez    = $amplitude_classical
# t0    = $t0_classical
# tau0  = $tau0_classical

# %TDExternalFields
#   electric_field | 1 | 0 | 0 | omega | "envelope_gauss"
# %

# %TDFunctions
#   "envelope_gauss" | tdf_gaussian | $amplitude_classical | tau0 | t0
# %

