#!/bin/bash

module load git

current_path_dir=`pwd`

dir_number=$(echo $current_path_dir | awk -F/ '{print NF}')
current_dir=$( echo $current_path_dir | cut -d'/' -f$dir_number)

nn=$(echo $current_dir | awk -F"-" '{print NF}')
current_octopus_svn=$( echo $current_dir | cut -d'-' -f$nn)

git log --stat > log_file
git_log_first_line=$( head -n 1 log_file | tail -n 1)
git_rev_number=$( echo $git_log_first_line | cut -d' ' -f2)
git_rev_list=$( ls /u/$USER/maxwell_tdks_octopus_executables/octopus_maxwell_svn-${current_octopus_svn} )
git_rev_array=($git_rev_list)
git_rev_count=${#git_rev_array[@]}
for ii in $( seq 1 $git_rev_count)
do
  jj=$( echo "$ii-1" | bc )
  git_rev_check=$( echo ${git_rev_array[$jj]} | cut -d'_' -f2 )
  if [[ $git_rev_check == $git_rev_number ]]
  then
    git_rev_folder=${git_rev_array[$jj]}
  else
    git_rev_count=$( echo "$git_rev_count+1" | bc )
    git_rev_count=$( printf "%03d" $git_rev_count )
    git_rev_folder=${git_rev_count}_${git_rev_number}
  fi
done

module purge
module load intel/16.0 mkl/11.3 mpi.intel/5.1.3 gsl/2.1 fftw/3.3.4

make clean
make distclean

autoreconf -i

./configure \
  CC=mpiicc \
  CFLAGS='-O3 -xavx -fma -ip -openmp -fzero-initialized-in-bss -init=arrays -init=zero' \
  FC=mpiifort \
  FCFLAGS='-O3 -xavx -fma -ip -openmp -fzero-initialized-in-bss -init=arrays -init=zero' \
  LDFLAGS='-L/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm -Xlinker -rpath=/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/lib/intel64:/u/system/SLES11/soft/gsl/2.1/gcc-5.4/lib:/u/system/SLES11/soft/pfft/1.0.7a/intel-16.0/mpi.ibm-1.4/lib:/u/system/SLES11/soft/netcdf/4.4.0/intel16.0/serial/lib' \
  --prefix=/u/$USER/maxwell_tdks_octopus_executables/octopus_maxwell_svn-${current_octopus_svn}/$git_rev_folder/ \
  --enable-mpi \
  --enable-openmp \
  --enable-avx \
  --disable-gdlib \
  --with-gsl-prefix=/u/system/SLES11/soft/gsl/2.1/gcc-5.4 \
  --with-libxc-prefix=/u/system/SLES11/soft/libxc/2.2.3/intel-16.0/mpi.ibm-1.4/ \
  --with-fftw-prefix=/u/system/SLES11/soft/fftw/3.3.4/intel-16.0/mpi.ibm-1.4 \
  --with-blas='-L/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-lapack='-L/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-blacs='-L/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-scalapack='-L/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-pfft-prefix=/u/system/SLES11/soft/pfft/1.0.7a/intel-16.0/mpi.ibm-1.4 \
  --with-netcdf-prefix=/u/system/SLES11/soft/netcdf/4.4.0/intel16.0/serial \
  --with-isf-prefix=/u/system/SLES11/soft/bigdft/1.7.7/intel-16.0/mpi.ibm-1.4 \
  --with-metis-prefix=/u/system/SLES11/soft/metis/5.1.0/intel-16.0 \
  --with-parmetis-prefix=/u/system/SLES11/soft/parmetis/4.0.3/intel-16.0/mpi.ibm-1.4

make -j
make install
