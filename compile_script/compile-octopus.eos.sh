#!/bin/bash

module load git

current_path_dir=`pwd`

dir_number=$(echo $current_path_dir | awk -F/ '{print NF}')
current_dir=$( echo $current_path_dir | cut -d'/' -f$dir_number)

nn=$(echo $current_dir | awk -F"-" '{print NF}')
current_octopus_svn=$( echo $current_dir | cut -d'-' -f$nn)

git log --stat > log_file
git_log_first_line=$( head -n 1 log_file | tail -n 1)
git_rev_number=$( echo $git_log_first_line | cut -d' ' -f2)
git_rev_list=$( ls /u/$USER/maxwell_tdks_octopus_executables/octopus_maxwell_svn-${current_octopus_svn} )
git_rev_array=($git_rev_list)
git_rev_count=${#git_rev_array[@]}
for ii in $( seq 1 $git_rev_count)
do
  jj=$( echo "$ii-1" | bc )
  git_rev_check=$( echo ${git_rev_array[$jj]} | cut -d'_' -f2 )
  if [[ $git_rev_check == $git_rev_number ]]
  then
    git_rev_folder=${git_rev_array[$jj]}
  else
    git_rev_count=$( echo "$git_rev_count+1" | bc )
    git_rev_count=$( printf "%03d" $git_rev_count )
    git_rev_folder=${git_rev_count}_${git_rev_number}
  fi
done

module purge
module load intel/15.0 mkl/11.2 impi/5.1.1 gsl/1.16 fftw/3.3.4 nfft/3.2.4

make clean
make distclean

autoreconf -i

./configure \
  CC=mpiicc \
  CFLAGS='-O3 -xCORE-AVX2 -fma -ip -openmp -fzero-initialized-in-bss -init=arrays -init=zero' \
  FC=mpiifort \
  FCFLAGS='-O3 -xCORE-AVX2 -fma -ip -openmp -fzero-initialized-in-bss -init=arrays -init=zero' \
  LDFLAGS='-L/afs/@cell/common/soft/intel/ics2015/15.0/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm -Xlinker -rpath=/afs/@cell/common/soft/intel/ics2015/15.0/mkl/lib/intel64:/afs/@cell/.cs/gsl/1.16/@sys/lib:/u/system/SLES11/soft/pfft/1.0.8a/intel-15.0/impi-5.1.1/lib:/afs/rzg/common/soft/netcdf/4.3.3.1/amd64_sles11/intel/15.0/serial/lib' \
  --prefix=/u/$USER/maxwell_tdks_octopus_executables/octopus_maxwell_svn-${current_octopus_svn}/$git_rev_folder/ \
  --enable-mpi \
  --enable-openmp \
  --enable-avx \
  --disable-gdlib \
  --with-gsl-prefix=/afs/@cell/.cs/gsl/1.16/@sys \
  --with-libxc-prefix=/u/system/SLES11/soft/libxc/2.2.3/intel-15.0/impi-5.1.1 \
  --with-fftw-prefix=/afs/rzg/.cs/fftw/fftw-3.3.4/@sys/intel-15.0/impi-5.0/ \
  --with-blas='-L/afs/@cell/common/soft/intel/ics2015/15.0/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-lapack='-L/afs/@cell/common/soft/intel/ics2015/15.0/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-blacs='-L/afs/@cell/common/soft/intel/ics2015/15.0/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-scalapack='-L/afs/@cell/common/soft/intel/ics2015/15.0/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-pfft-prefix=/u/system/SLES11/soft/pfft/1.0.8a/intel-15.0/impi-5.1.1 \
  --with-netcdf-prefix=/afs/rzg/common/soft/netcdf/4.3.3.1/amd64_sles11/intel/15.0/serial \
  --with-isf-prefix=/u/mjr/soft/rzg_software_stack/SLES11/octopus/bigdft/1.7.6 \
  --with-metis-prefix=/u/mjr/soft/rzg_software_stack/SLES11/octopus/metis/5.1.0 \
  --with-parmetis-prefix=/u/mjr/soft/rzg_software_stack/SLES11/octopus/parmetis/4.0.3 \

make -j
make install

