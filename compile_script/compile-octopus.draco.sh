#!/bin/bash

current_path_dir=`pwd`

dir_number=$(echo $current_path_dir | awk -F/ '{print NF}')
current_dir=$( echo $current_path_dir | cut -d'/' -f$dir_number)

nn=$(echo $current_dir | awk -F"-" '{print NF}')
current_octopus_svn=$( echo $current_dir | cut -d'-' -f$nn)

git log --stat > log_file
git_log_first_line=$( head -n 1 log_file | tail -n 1)
git_rev_number=$( echo $git_log_first_line | cut -d' ' -f2)
git_rev_list=$( ls /u/$USER/maxwell_tdks_octopus_executables/octopus_maxwell_svn-${current_octopus_svn} )
git_rev_array=($git_rev_list)
git_rev_count=${#git_rev_array[@]}
for ii in $( seq 1 $git_rev_count)
do
  jj=$( echo "$ii-1" | bc )
  git_rev_check=$( echo ${git_rev_array[$jj]} | cut -d'_' -f2 )
  if [[ $git_rev_check == $git_rev_number ]]
  then
    git_rev_folder=${git_rev_array[$jj]}
  else
    git_rev_count=$( echo "$git_rev_count+1" | bc )
    git_rev_count=$( printf "%03d" $git_rev_count )
    git_rev_folder=${git_rev_count}_${git_rev_number}
  fi
done

module purge
module load intel/16.0 mkl/11.3 impi/5.1.3 gsl/2.1 fftw/3.3.4

make clean
make distclean

autoreconf -i

./configure \
  CC=mpiicc \
  CFLAGS='-O3 -xCORE-AVX2 -fma -ip -openmp -fzero-initialized-in-bss -init=arrays -init=zero' \
  FC=mpiifort \
  FCFLAGS='-O3 -xCORE-AVX2 -fma -ip -openmp -fzero-initialized-in-bss -init=arrays -init=zero' \
  LDFLAGS='-L/mpcdf/soft/SLES114/common/intel/ps2016.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm -Xlinker -rpath=/mpcdf/soft/SLES114/common/intel/ps2016.3/16.0/linux/mkl/lib/intel64:/mpcdf/soft/SLES114/HSW/gsl/2.1/gcc-5.4/lib:/mpcdf/soft/SLES114/HSW/pfft/1.0.8a/intel-16.0/impi-5.1/lib:/mpcdf/soft/SLES114/common/netcdf/4.4.0/intel16.0/serial/lib' \
  --prefix=/u/$USER/maxwell_tdks_octopus_executables/octopus_maxwell_svn-${current_octopus_svn}/$git_rev_folder/ \
  --enable-mpi \
  --enable-openmp \
  --enable-avx \
  --disable-gdlib \
  --with-gsl-prefix=/mpcdf/soft/SLES114/HSW/gsl/2.1/gcc-5.4 \
  --with-libxc-prefix=/mpcdf/soft/SLES114/HSW/libxc/2.2.3/intel-16.0/impi-5.1 \
  --with-fftw-prefix=/mpcdf/soft/SLES114/HSW/fftw/3.3.4/intel-16.0/impi-5.1 \
  --with-blas='-L/mpcdf/soft/SLES114/common/intel/ps2016.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-lapack='-L/mpcdf/soft/SLES114/common/intel/ps2016.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-blacs='-L/mpcdf/soft/SLES114/common/intel/ps2016.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-scalapack='-L/mpcdf/soft/SLES114/common/intel/ps2016.3/16.0/linux/mkl/lib/intel64 -lmkl_scalapack_lp64 -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lmkl_blacs_intelmpi_lp64 -lpthread -lm' \
  --with-pfft-prefix=/mpcdf/soft/SLES114/HSW/pfft/1.0.8a/intel-16.0/impi-5.1 \
  --with-netcdf-prefix=/mpcdf/soft/SLES114/common/netcdf/4.4.0/intel16.0/serial \
  --with-isf-prefix=/mpcdf/soft/SLES114/HSW/bigdft/1.7.7/intel-16.0/impi-5.1 \
  --with-metis-prefix=/mpcdf/soft/SLES114/HSW/metis/5.1.0/intel-16.0 \
  --with-parmetis-prefix=/mpcdf/soft/SLES114/HSW/parmetis/4.0.3/intel-16.0/impi-5.1

make -j
make install

# --with-mpifftw-prefix=/mpcdf/soft/SLES114/HSW/pfft/fftw4pfft/3.3.4/intel-16.0/impi-5.1 \
