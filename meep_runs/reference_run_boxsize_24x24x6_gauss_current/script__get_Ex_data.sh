#/bin/bash

time_step=0.0
if [ -e Ex.dat ]
then
  rm Ex.dat
fi
while read line
do
  first_element=$( echo $line | cut -d' ' -f1)
  if [ "$first_element" == "Ex" ]
  then
    time_step=$( echo " $time_step + 0.1 " | bc -l )
    Ex=$( echo $line | cut -d' ' -f3)
    echo $time_step $Ex >> Ex.dat
  fi
done < reference.out
