#/bin/bash

time_step=0.0
if [ -e Hx.dat ]
then
  rm Hx.dat
fi
while read line
do
  first_element=$( echo $line | cut -d' ' -f1)
  if [ "$first_element" == "Hx" ]
  then
    time_step=$( echo " $time_step + 0.1 " | bc -l )
    Hx=$( echo $line | cut -d' ' -f3)
    echo $time_step $Hx >> Hx.dat
  fi
done < reference.out
