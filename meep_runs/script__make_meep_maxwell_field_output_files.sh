#/bin/bash

time_dt=0.1

time_factor=1.0
E_field_factor=1.0
H_field_factor=1.0

field_list=" Ex Ey Ez Hx Hy Hz "

field_array=($field_list)

for ii in $( seq 0 $( echo "${#field_array[@]}-1" | bc ) )
do 

  file_name=${field_array[$ii]}.dat
  if [ -e $file_name ]
  then
   rm $file_name
  fi
  time_value=0.0
  while read line
  do
    field=$( echo $line | cut -d' ' -f1)
    if [[ $field == ${field_array[$ii]} ]]
    then
      time_value=$( echo " $time_value + $time_dt * $time_factor " | bc -l )
      field_value=$( echo $line | cut -d' ' -f3)
      echo $time_value $field_value >> $file_name
    fi
  done < meep.out

done



#if [ -e Ez.dat ]
#then
#  rm Ez.dat
#fi
#while read line
#do
#  first_element=$( echo $line | cut -d' ' -f1)
#  if [ "$first_element" == "Ez" ]
#  then
#    time_step=$( echo " $time_step + 0.1 " | bc -l )
#    Ez=$( echo $line | cut -d' ' -f3)
#    echo $time_step $Ez >> Ez.dat
#  fi
#done < meep.out

