#/bin/bash

time_step=0.0
if [ -e Ez.dat ]
then
  rm Ez.dat
fi
while read line
do
  first_element=$( echo $line | cut -d' ' -f1)
  if [ "$first_element" == "Ez" ]
  then
    time_step=$( echo " $time_step + 0.1 " | bc -l )
    Ez=$( echo $line | cut -d' ' -f3)
    echo $time_step $Ez >> Ez.dat
  fi
done < reference.out
