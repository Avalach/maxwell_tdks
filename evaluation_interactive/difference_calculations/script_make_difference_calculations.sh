#!/bin/bash

pushd ./density_difference/density_difference.x=0/
  ./make_density_difference_filename_list.x=0.sh
  ./difference_calculation.x
popd

pushd ./maxwell_e_field_difference/maxwell_e_field_difference-z.z=0
  ./make_maxwell_e_field_difference-z_filename_list.z=0.sh
  ./difference_calculation.x
popd
