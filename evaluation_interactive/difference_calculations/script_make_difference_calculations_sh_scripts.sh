#!/bin/bash

# folder
main_folder_scalar_td_0="density maxwell_energy_density"
main_folder_field_td_all="maxwell_b_field maxwell_e_field"

# difference folders
main_difference_folder_scalar="density_difference maxwell_energy_density_difference"
main_difference_folder_field="maxwell_b_field_difference maxwell_e_field_difference"

# direction
plot_direction="x y z"
field_direction="x y z"


###################################################################################################

# build arrays
folder_scalar_array_td_0=($main_folder_scalar_td_0)
folder_field_array_td_all=($main_folder_field_td_all)
difference_folder_scalar_array=($main_difference_folder_scalar)
difference_folder_field_array=($main_difference_folder_field)


###################################################################################################
# script for main_folder_scalar_td_0 

(( seq_limit_ii=${#folder_scalar_array_td_0[@]}-1 ))

for ii in `seq 0 $seq_limit_ii`
do
  mkdir ${difference_folder_scalar_array[$ii]}
  for jj in $plot_direction
  do
    folder=${difference_folder_scalar_array[$ii]}/${difference_folder_scalar_array[$ii]}.$jj=0
    mkdir $folder
    script_file="$folder/make_${difference_folder_scalar_array[$ii]}_filename_list.${jj}=0.sh"
    cp ./run_dir_list_td_0 ./$folder/run_dir_list
    cp ../difference_calc_program/difference_calculation.f90 ./$folder
    cp ../difference_calc_program/difference_calculation.x ./$folder

    cat <<- EOF > $script_file
#!/bin/bash

rm filename_list

while read run_dir
do

  run_dir_1=\$( echo \$run_dir | awk -F" " '{print \$1}')
  run_dir_2=\$( echo \$run_dir | awk -F" " '{print \$2}')
  count=0
  for ii in \$run_dir_1/output_iter/td.*/${folder_scalar_array_td_0[$ii]}.$jj=0 
  do
    if [ \$count -ne 0 ] 
    then
      run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
      td_number=\$( echo "\$run_dir_number_1-1" | bc)
      td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
      jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
      ii_2=\$run_dir_2/output_iter/td.0000000/${folder_scalar_array_td_0[$ii]}.$jj=0
      ii_3=\$run_dir_1/output_iter/td.\$jj/${difference_folder_scalar_array[$ii]}.$jj=0
      echo \'\$ii\'  "     '\$ii_2'     '\$ii_3'  "  >> filename_list
    fi
    count=\$(( \$count+1 ))
  done

done < run_dir_list

EOF

    chmod 777 $script_file
  done
done


###################################################################################################
# script for main_folder_field_td_all

(( seq_limit_ii=${#folder_field_array_td_all[@]}-1 ))

for ii in `seq 0 $seq_limit_ii`
do
  mkdir ${difference_folder_field_array[$ii]}
  for jj in $plot_direction
  do
    for kk in $field_direction
    do
      folder=${difference_folder_field_array[$ii]}/${difference_folder_field_array[$ii]}-$kk.$jj=0
      mkdir $folder
      script_file="$folder/make_${difference_folder_field_array[$ii]}-${kk}_filename_list.${jj}=0.sh"
      cp ./run_dir_list_td_all ./$folder/run_dir_list
      cp ../difference_calc_program/difference_calculation.f90 ./$folder
      cp ../difference_calc_program/difference_calculation.x ./$folder

      cat <<- EOF > $script_file
#!/bin/bash

rm filename_list

while read run_dir
do

  run_dir_1=\$( echo \$run_dir | awk -F" " '{print \$1}')
  run_dir_2=\$( echo \$run_dir | awk -F" " '{print \$2}')
  count=0
  for ii in \$run_dir_1/output_iter/td.*/${folder_field_array_td_all[$ii]}-$kk.$jj=0 
  do
    if [ \$count -ne 0 ] 
    then
      run_dir_number_1=\$(echo \$ii | awk -F'/' '{print NF}')
      td_number=\$( echo "\$run_dir_number_1-1" | bc)
      td_dir=\$( echo \$ii | cut -d'/' -f \$td_number )
      jj=\$( echo \$td_dir | awk -F'.' '{print \$2}')
      ii_2=\$run_dir_2/output_iter/td.\$jj/${folder_field_array_td_all[$ii]}-$kk.$jj=0
      ii_3=\$run_dir_1/output_iter/td.\$jj/${difference_folder_field_array[$ii]}-$kk.$jj=0
      echo \'\$ii\'  "     '\$ii_2'     '\$ii_3'  "  >> filename_list
    fi
    count=\$(( \$count+1 ))
  done

done < run_dir_list

EOF

      chmod 777 $script_file
    done
  done
done

