#!/bin/bash

pushd ./maxwell_e_field/maxwell_e_field-z.z=0/
  ./figures_script_maxwell_e_field-z.z=0.sh
popd

pushd ./maxwell_e_field_difference/maxwell_e_field_difference-z.z=0/
  ./figures_script_maxwell_e_field_difference-z.z=0.sh
popd

pushd ./density/density.x=0/
  ./figures_script_density.x=0.sh
popd

pushd ./density_difference/density_difference.x=0/
  ./figures_script_density_difference.x=0.sh
popd

pushd ./current/current-x.x=0/
  ./figures_script_current-x.x=0.sh
popd
