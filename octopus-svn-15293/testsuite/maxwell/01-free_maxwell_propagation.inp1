
DebugLevel                        = 1
ProfilingMode                     = yes

CalculationMode                   = maxwell_ks
ParallelizationStrategy           = par_domains


# ----- Matter box variables ----------------------------------------------------------------------

Dimensions                        = 3
BoxShape                          = PARALLELEPIPED
%Lsize
 012.00 | 012.00 | 012.00
%
%Spacing
 0.25 | 0.25 | 0.25
%


# ----- Maxwell box variables ---------------------------------------------------------------------

MaxwellDimensions                 = 3
MaxwellBoxShape                   = PARALLELEPIPED

%MaxwellLsize
 012.00 | 012.00 | 012.00
%

%MaxwellSpacing
 0.25 | 0.25 | 0.25
%


# ----- Species variables -------------------------------------------------------------------------

%Species
'Na'       | species_pseudo         | file | '/u/rej/octopus_maxwell/codes/16-03-16_octopus_maxwell_svn-13931/share/pseudopotentials/PSF//Na.psf'
%

%Coordinates
 'Na' | 0 | 0 | -1.53945
 'Na' | 0 | 0 |  1.53945
%


# ----- Matter calculation variables --------------------------------------------------------------

Extrastates                       = 2

#XCFunctional                     = gga_x_pbe_r + gga_c_pbe
XCFunctional                      = lda_x + lda_c_gl

EigenSolver                       = cg
EigenSolverTolerance              = 1.00e-12
EigensolverMaxIter                = 50
ConvRelDens                       = 1.00e-12
MaximumIter                       = 1000

TDExpOrder                        = 4
DerivativesStencil                = stencil_starplus
DerivativesOrder                  = 8

AbsorbingBoundaries               = mask
ABWidth                           = 2.0


# ----- Maxwell calculation variables -------------------------------------------------------------

MaxwellHamiltonianOperator        = spin_matrix
MaxwellTDOperatorMethod           = maxwell_op_fd
MaxwellTDPropagator               = maxwell_etrs

MatterToMaxwellCoupling           = no
MaxwellToMatterCoupling           = no
MaxwellCouplingOrder              = electric_dipole_coupling

MaxwellTDExpOrder                 = 4
MaxwellDerivativesStencil         = stencil_starplus
MaxwellDerivativesOrder           = 8

MaxwellFreePropagationTest        = no
MaxwellTDEnergyCalculationIter    = 1

MaxwellIncidentWavesEnergyCalc    = yes
MaxwellIncidentWavesEnergyCalcIter= 10

MaxwellAbsorbingBoundaries        = mask
MaxwellABWidth                    = 2.0

MaxwellTDTransEfieldCalculation   = no
MaxwellTDEfieldTransVariance      = no
MaxwellTDEfieldTransVarianceIter  = 10
MaxwellTDEfieldDivergence         = no
MaxwellTDEfieldDivergenceIter     = 10


# ----- Output variables --------------------------------------------------------------------------

OutputHow                         = plane_x + plane_y + plane_z + vtk + cube


# ----- Matter output variables -------------------------------------------------------------------

Output                            = density + current + geometry + forces
OutputInterval                    = 1
TDOutput                          = energy + maxwell_energy_density + geometry + multipoles


# ----- Maxwell output variables ------------------------------------------------------------------

MaxwellOutput                     = electric_field + magnetic_field
MaxwellOutputInterval             = 1
MaxwellTDOutput                   = maxwell_energy + maxwell_fields


# ----- Time step variables -----------------------------------------------------------------------

TDTimeStep                        = 0.00250
TDMaxSteps                        = 10
TDEnergyUpdateIter                = 1


# ----- Maxwell field variables -------------------------------------------------------------------
pi=3.14159265359
c=137.035999679

# ----- laser propagates in x direction ---------
lambda= 10.00/4
omega = 2 * pi * c / lambda
kx    = omega / c
Ez    = 1.00000
sx    = lambda
sy    = lambda
dx_1  = -10.0
dx_2  =   5.0

%UserDefinedMaxwellIncidentWaves
  incident_wave_gauss_pulse | "0" | "0" | "Ez" | "omega" | "sx" | "dx_1" | neg_dir | no | no | plane_wave
%

%UserDefinedMaxwellStates
 1 | formula | " 0  " | "  0   "
 2 | formula | " 0  " | " -1/c * Ez * (cos(kx*(x-dx_2)) * exp(-((x-dx_2)/sx)^2) * exp(-(y/sy)^2) + cos(kx*(x-dx_1)) * exp(-((x-dx_1)/sx)^2))  "
 3 | formula | " Ez * (cos(kx*(x-dx_2)) * exp(-((x-dx_2)/sx)^2) * exp(-(y/sy)^2) + cos(kx*(x-dx_1)) * exp(-((x-dx_1)/sx)^2)) " | "  0  "
%


