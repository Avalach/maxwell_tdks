# ----- Maxwell_ks mode ---------------------------------------------------------------------------

 CalculationMode                   = maxwell_ks

 ParDomains                        = auto
 ParStates                         = no


# ----- Matter box variables ----------------------------------------------------------------------

 Dimensions                        = 3
 BoxShape                          = PARALLELEPIPED
 %Lsize
  010.00 | 010.00 | 010.00
 %
 %Spacing
  0.50 | 0.50 | 0.50
 %


# ----- Maxwell box variables ---------------------------------------------------------------------

 MaxwellDimensions                 = 3
 MaxwellBoxShape                   = PARALLELEPIPED

 %MaxwellLsize
  1000.00 | 1000.00 | 1000.00
 %

 %MaxwellSpacing
  50.00 | 50.00 | 50.00
 %


# ----- Species variables -------------------------------------------------------------------------

 %Species
 'Na'       | species_pseudo         | file | '/home/jestaedt/maxwell_tdks/octopus-svn-trunk/share/pseudopotentials/PSF//Na.psf'
 %

 %Coordinates
  'Na' | 0 | 0 | -1.53945
  'Na' | 0 | 0 |  1.53945
 %


# ----- Matter calculation variables --------------------------------------------------------------

 Extrastates                       = 2

 XCFunctional                      = lda_x + lda_c_gl

 EigenSolver                       = cg
 EigenSolverTolerance              = 1.00e-12
 EigensolverMaxIter                = 50
 ConvRelDens                       = 1.00e-12
 MaximumIter                       = 1000

 TDExpOrder                        = 4
 DerivativesStencil                = stencil_starplus
 DerivativesOrder                  = 8

 AbsorbingBoundaries               = mask
 ABWidth                           = 1.5


# ----- Maxwell calculation variables -------------------------------------------------------------

 MaxwellHamiltonianOperator        = spin_matrix
 MaxwellTDOperatorMethod           = maxwell_op_fd
 MaxwellTDPropagator               = maxwell_etrs

 MatterToMaxwellCoupling           = no
 MaxwellToMatterCoupling           = yes
 MaxwellCouplingOrder              = electric_dipole_coupling

 MaxwellTDExpOrder                 = 4
 MaxwellDerivativesStencil         = stencil_starplus
 MaxwellDerivativesOrder           = 8

 MaxwellFreePropagationTest        = no
 MaxwellTDEnergyCalculationIter    = 1

 MaxwellIncidentWavesEnergyCalc    = no
 MaxwellIncidentWavesEnergyCalcIter= 10

 MaxwellABWidth                    = 1.5
 MaxwellAbsorbingBoundaries        = no

 MaxwellTDTransEfieldCalculation   = no
 MaxwellTDEfieldTransVariance      = no
 MaxwellTDEfieldTransVarianceIter  = 10
 MaxwellTDEfieldDivergence         = no
 MaxwellTDEfieldDivergenceIter     = 10


# ----- Output variables --------------------------------------------------------------------------

 OutputFormat                      = plane_x + plane_y + plane_z + vtk + cube + xyz


# ----- Matter output variables -------------------------------------------------------------------

 Output                            = density + current + geometry + forces + elf
 OutputInterval                    = 1
 TDOutput                          = energy + maxwell_energy_density + geometry + multipoles


# ----- Maxwell output variables ------------------------------------------------------------------

 MaxwellOutput                     = electric_field + magnetic_field
 MaxwellOutputInterval             = 1
 MaxwellTDOutput                   = maxwell_energy  + maxwell_fields

 %MaxwellFieldsCoordinate
   0.00 | 0.00 | 0.00
 %

# ----- Time step variables -----------------------------------------------------------------------

 TDTimeStep                        = 0.1
 TDMaxSteps                        = 10
 TDEnergyUpdateIter                = 1


# ----- Maxwell field variables -------------------------------------------------------------------
 pi=3.14159265359
 c=137.035999679


# ----- laser propagates in x direction ---------
 lambda= 5000.00
 omega = 2 * pi * c / lambda
 kx    = omega / c
 Ez    = 1.00000
 sx    = lambda
 dx    = 0.0


 %UserDefinedMaxwellIncidentWaves
   incident_wave_gauss_pulse | "kx" | "0" | "0" | "0" | "0" | "Ez" | "sx" | "dx" | plane_wave
 %

 %UserDefinedMaxwellStates
  1 | formula | " 0                                           " | "  0                                                 "
  2 | formula | " 0                                           " | " -1/c * Ez * cos(kx*(x-dx)) *  exp(-((x-dx)/sx)^2)  "
  3 | formula | " Ez * cos(kx*(x-dx)) *  exp(-((x-dx)/sx)^2)  " | "  0                                                 "
