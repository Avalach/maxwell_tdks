!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!
!! $Id: output_maxwell.F90 15722 2016-11-09 13:40:45Z nicolastd $


  ! ---------------------------------------------------------
  subroutine maxwell_output_init(maxwell_outp, maxwell_sb, nst)
    type(output_t),       intent(out) :: maxwell_outp
    type(simul_box_t),    intent(in)  :: maxwell_sb
    integer,              intent(in)  :: nst

    PUSH_SUB(maxwell_output_init)

    !%Variable MaxwellOutput
    !%Type flag
    !%Default none
    !%Section Output
    !%Description
    !% Specifies what to print. The output files are written at the end of the run into the output directory for the
    !% Maxwell run.
    !% Time-dependent simulations print only per iteration, including always the last. The frequency of output per iteration
    !% is set by <tt>OutputInterval</tt> and the directory is set by <tt>OutputIterDir</tt>.
    !%Option maxwell_electric_field bit(0)
    !% Output of the electric field
    !%Option maxwell_magnetic_field bit(1)
    !% Output of the magnetic field
    !%Option maxwell_trans_electric_field bit(2)
    !% Output of the transversal electric field
    !%Option maxwell_trans_magnetic_field bit(3)
    !% Output of the transversal magnetic field
    !%Option maxwell_long_electric_field bit(4)
    !% Output of the longitudinal electric field
    !%Option maxwell_long_magnetic_field bit(5)
    !% Output of the longitudinal magnetic field
    !%Option maxwell_div_electric_field bit(6)
    !% Output of the divergence of the electric field
    !%Option maxwell_div_magnetic_field bit(7)
    !% Output of the divergence of the magnetic field
    !%Option maxwell_vector_potential bit(8)
    !% Output of the vector potential
    !%Option maxwell_poynting_vector bit(9)
    !% Output of the Maxwell Poynting vector
    !%Option maxwell_energy_density bit(10)
    !% Output of the electromagnetic density
    !%Option maxwell_current bit(11)
    !% Output of the Maxwell current i.e. matter current plus external current.
    !%Option maxwell_external_current bit(12)
    !% Output of the external Maxwell current
    !%Option maxwell_electric_dipole_potential bit(13)
    !% Output of the electric dipole potential
    !%Option maxwell_electric_quadrupole_potential bit(14)
    !% Output of the electric quadrupole potential
    !%Option maxwell_charge_density bit(15)
    !% Output of the charge density calculated by the divergence of the electric field.
    !%Option maxwell_charge_density_diff bit(16)
    !% Output of the charge density difference, one calculated via matter calculation, the other
    !% one calculated with Div(E) = rho/epsilon.
    !%Option maxwell_test_output bit(17)
    !% Output of a test function for debugging
    !%End
    call parse_variable('MaxwellOutput', 0, maxwell_outp%what)

    !%Variable MaxwellOutputInterval
    !%Type integer
    !%Default 50
    !%Section Output
    !%Description
    !% The output requested by variable <tt>MaxwellOutput</tt> is written
    !% to the directory <tt>MaxwellOutputIterDir</tt>
    !% when the iteration number is a multiple of the <tt>OutputInterval</tt> variable.
    !% Subdirectories are named Y.X, where Y is <tt>td</tt>, <tt>scf</tt>, or <tt>unocc</tt>, and
    !% X is the iteration number. To use the working directory, specify <tt>"."</tt>
    !% (Output of restart files is instead controlled by <tt>RestartWriteInterval</tt>.)
    !% Must be >= 0. If it is 0, then no output is written. 
    !%End
    call parse_variable('MaxwellOutputInterval', 50, maxwell_outp%output_interval)
    if(maxwell_outp%output_interval < 0) then
      message(1) = "OutputInterval must be >= 0."
      call messages_fatal(1)
    endif

    !%Variable MaxwellOutputInterval
    !%Type integer
    !%Default 50
    !%Section Output
    !%Description
    !% The output requested by variable <tt>MaxwellOutput</tt> is written
    !% to the directory <tt>MaxwellOutputIterDir</tt>
    !% when the iteration number is a multiple of the <tt>MaxwellOutputInterval</tt> variable.
    !% Subdirectories are named Y.X, where Y is <tt>td</tt>, <tt>scf</tt>, or <tt>unocc</tt>, and
    !% X is the iteration number. To use the working directory, specify <tt>"."</tt>
    !% (Output of restart files is instead controlled by <tt>MaxwellRestartWriteInterval</tt>.)
    !% Must be >= 0. If it is 0, then no output is written. 
    !%End
    call parse_variable('MaxwellOutputInterval', 50, maxwell_outp%output_interval)
    if(maxwell_outp%output_interval < 0) then
      message(1) = "MaxwellOutputInterval must be >= 0."
      call messages_fatal(1)
    endif

    !%Variable MaxwellOutputIterDir
    !%Default "output_iter"
    !%Type string
    !%Section Output
    !%Description
    !% The name of the directory where <tt>Octopus</tt> stores information
    !% such as the density, forces, etc. requested by variable <tt>MaxwellOutput</tt>
    !% in the format specified by <tt>OutputHow</tt>.
    !% This information is written while iterating <tt>CalculationMode = maxwell</tt>
    !% according to <tt>OutputInterval</tt>, and has nothing to do with the restart information.
    !%End
    call parse_variable('MaxwellOutputIterDir', "output_iter", maxwell_outp%iter_dir)
    if(maxwell_outp%what /= 0 .and. maxwell_outp%output_interval > 0) then
      call io_mkdir(maxwell_outp%iter_dir)
    endif
    call add_last_slash(maxwell_outp%iter_dir)

    !%Variable MaxwellRestartWriteInterval
    !%Type integer
    !%Default 50
    !%Section Execution::IO
    !%Description
    !% Restart data is written when the iteration number is a multiple of the
    !% <tt>MaxwellRestartWriteInterval</tt> variable. (Other output is controlled by <tt>MaxwellOutputInterval</tt>.)
    !%End
    call parse_variable('MaxwellRestartWriteInterval', 50, maxwell_outp%restart_write_interval)
    if(maxwell_outp%restart_write_interval <= 0) then
      message(1) = "MaxwellRestartWriteInterval must be > 0."
      call messages_fatal(1)
    endif

    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_ELECTRIC_FIELD) /= 0) then
      maxwell_outp%wfs_list = trim("1-3")
    end if

    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_MAGNETIC_FIELD) /= 0) then
      maxwell_outp%wfs_list = trim("1-3")
    end if

    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_TRANS_ELECTRIC_FIELD) /= 0) then
      maxwell_outp%wfs_list = trim("1-3")
    end if

    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_TRANS_MAGNETIC_FIELD) /= 0) then
      maxwell_outp%wfs_list = trim("1-3")
    end if

    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_CURRENT) /= 0) then
      maxwell_outp%wfs_list = trim("1-3")
    end if

    call io_function_read_how(maxwell_sb, maxwell_outp%how)

    POP_SUB(maxwell_output_init)
  end subroutine maxwell_output_init


  ! ---------------------------------------------------------
  subroutine maxwell_output(maxwell_outp, maxwell_gr, maxwell_st, maxwell_hm, time, geo, dir, gr, st, hm)
    type(output_t),                intent(in)    :: maxwell_outp
    type(states_t),                intent(inout) :: maxwell_st
    type(hamiltonian_t),           intent(in)    :: maxwell_hm
    type(grid_t),                  intent(inout) :: maxwell_gr
    FLOAT,                         intent(in)    :: time
    type(geometry_t),              intent(inout) :: geo
    character(len=*),              intent(in)    :: dir
    type(grid_t),        optional, intent(inout) :: gr
    type(states_t),      optional, intent(inout) :: st
    type(hamiltonian_t), optional, intent(in)    :: hm

    PUSH_SUB(maxwell_output)

    if(maxwell_outp%what /= 0) then
      message(1) = "Info: Writing output to " // trim(dir)
      call messages_info(1)
      call io_mkdir(dir)
    endif

    call maxwell_output_maxwell_states(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    call maxwell_output_energy_density(maxwell_hm, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_poynting_vector(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    call maxwell_output_transverse_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_longitudinal_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_divergence_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_vector_potential(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    call maxwell_output_coupling_potentials(hm, gr, geo, dir, maxwell_outp)
    call maxwell_output_current_density(maxwell_st, maxwell_gr, maxwell_hm, st, gr, hm, geo, dir, time, maxwell_outp)
    call maxwell_output_external_current_density(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, time, maxwell_outp)
    if (present(st) .and. present(gr)) &
        call maxwell_output_charge_density(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp, st=st, gr=gr, hm=hm)
    call maxwell_output_test_output(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)

    POP_SUB(maxwell_output)
  end subroutine maxwell_output


  ! ---------------------------------------------------------
  subroutine maxwell_output_free(maxwell_outp, maxwell_gr, maxwell_st, maxwell_hm, time, geo, dir, gr, st, hm)
    type(output_t),                intent(in)    :: maxwell_outp
    type(states_t),                intent(inout) :: maxwell_st
    type(hamiltonian_t),           intent(in)    :: maxwell_hm
    type(grid_t),                  intent(inout) :: maxwell_gr
    FLOAT,                         intent(in)    :: time
    type(geometry_t),              intent(inout) :: geo
    character(len=*),              intent(in)    :: dir
    type(grid_t),        optional, intent(inout) :: gr
    type(states_t),      optional, intent(inout) :: st
    type(hamiltonian_t), optional, intent(in)    :: hm

    PUSH_SUB(maxwell_output_free)

    if(maxwell_outp%what /= 0) then
      message(1) = "Info: Writing output to " // trim(dir)
      call messages_info(1)
      call io_mkdir(dir)
    endif

    call maxwell_output_maxwell_states(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    call maxwell_output_energy_density(maxwell_hm, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_poynting_vector(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    call maxwell_output_transverse_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_longitudinal_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_divergence_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)
    call maxwell_output_vector_potential(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    call maxwell_output_external_current_density(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, time, maxwell_outp)
    call maxwell_output_charge_density(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    call maxwell_output_test_output(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)

    POP_SUB(maxwell_output_free)
  end subroutine maxwell_output_free


  ! ---------------------------------------------------------
  subroutine maxwell_output_maxwell_states(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    type(states_t),         intent(inout) :: maxwell_st
    type(grid_t),           intent(inout) :: maxwell_gr
    type(hamiltonian_t),    intent(in)    :: maxwell_hm
    type(geometry_t),       intent(in)    :: geo
    character(len=*),       intent(in)    :: dir
    type(output_t),         intent(in)    :: maxwell_outp

    integer :: idim, ierr
    character(len=MAX_PATH_LEN) :: fname
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:,:)

    PUSH_SUB(maxwell_output_maxwell_states)

    ! Electric field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_ELECTRIC_FIELD) /= 0) then
      fn_unit = units_out%energy/units_out%length
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
      if (.not. maxwell_hm%ma_mx_coupling) then
        call maxwell_get_electric_field_state(maxwell_st%maxwell_rs_state_trans+maxwell_st%maxwell_rs_state_long, dtmp, &
                                        maxwell_st%maxwell_ep, maxwell_gr%mesh%np_part)
      else
        call maxwell_get_electric_field_state(maxwell_st%maxwell_rs_state, dtmp, maxwell_st%maxwell_ep, maxwell_gr%mesh%np_part)
      end if
      do idim = 1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_e_field-', index2axis(idim)
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
      end do
      if (maxwell_hm%maxwell_plane_waves) then
        call maxwell_get_electric_field_state(maxwell_st%maxwell_rs_state_plane_waves, dtmp, maxwell_st%maxwell_ep, maxwell_gr%mesh%np_part)
        do idim = 1, maxwell_st%d%dim
          write(fname, '(2a)') 'free_maxwell_e_field-', index2axis(idim)
          call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
        end do
      end if
      SAFE_DEALLOCATE_A(dtmp)
    end if

    ! Magnetic field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_MAGNETIC_FIELD) /= 0) then
      fn_unit = unit_one/units_out%length**2
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
      call maxwell_get_magnetic_field_state(maxwell_st%maxwell_rs_state, maxwell_st%maxwell_rs_sign, dtmp, maxwell_st%maxwell_mu, &
                                            maxwell_gr%mesh%np_part)
      do idim = 1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_b_field-', index2axis(idim)
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
      end do
      if (maxwell_hm%maxwell_plane_waves) then
        call maxwell_get_magnetic_field_state(maxwell_st%maxwell_rs_state_plane_waves, maxwell_st%maxwell_rs_sign, dtmp, maxwell_st%maxwell_mu, &
                                              maxwell_gr%mesh%np_part)
        do idim = 1, maxwell_st%d%dim
          write(fname, '(2a)') 'free_maxwell_e_field-', index2axis(idim)
          call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
        end do
      end if
      SAFE_DEALLOCATE_A(dtmp)
    end if

    POP_SUB(maxwell_output_maxwell_states)
  end subroutine maxwell_output_maxwell_states  


  !----------------------------------------------------------
  subroutine maxwell_output_energy_density(maxwell_hm, maxwell_gr, geo, dir, maxwell_outp)   !< have to set unit output correctly
    type(hamiltonian_t),       intent(in)    :: maxwell_hm
    type(grid_t),              intent(in)    :: maxwell_gr
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer :: ierr

    PUSH_SUB(maxwell_output_energy_density)

    ! Maxell energy density
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_ENERGY_DENSITY) /= 0) then
      call dio_function_output(maxwell_outp%how, dir, "maxwell_energy_density", maxwell_gr%mesh, & 
                               maxwell_hm%maxwell_energy_density(:), units_out%energy/units_out%length**3, ierr, geo = geo)
      if (maxwell_hm%maxwell_plane_waves) then
      call dio_function_output(maxwell_outp%how, dir, "free_maxwell_energy_density", maxwell_gr%mesh, & 
                               maxwell_hm%maxwell_energy_density_plane_waves(:), units_out%energy/units_out%length**3, ierr, geo = geo)
      end if
    end if

    POP_SUB(maxwell_output_energy_density)
  end subroutine maxwell_output_energy_density


  !----------------------------------------------------------
  subroutine maxwell_output_poynting_vector(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)
    type(states_t),         intent(inout) :: maxwell_st
    type(grid_t),           intent(inout) :: maxwell_gr
    type(hamiltonian_t),    intent(in)    :: maxwell_hm
    type(geometry_t),       intent(in)    :: geo
    character(len=*),       intent(in)    :: dir
    type(output_t),         intent(in)    :: maxwell_outp

    integer :: idim, ierr
    character(len=MAX_PATH_LEN) :: fname
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:,:)

    PUSH_SUB(maxwell_output_poynting_vector)

    ! Maxwell Poynting vector
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_POYNTING_VECTOR) /= 0) then
      fn_unit = units_out%energy/(units_out%time*units_out%length**2)
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim))
       call maxwell_get_poynting_vector(maxwell_gr, maxwell_st, maxwell_st%maxwell_rs_state, maxwell_st%maxwell_rs_sign, &
                                        dtmp, maxwell_st%maxwell_ep, maxwell_st%maxwell_mu)
       do idim = 1, maxwell_st%d%dim
         write(fname, '(2a)') 'maxwell_poynting_vector-', index2axis(idim)
         call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
      end do
      if (maxwell_hm%maxwell_plane_waves) then
        call maxwell_get_poynting_vector_plane_waves(maxwell_gr, maxwell_st, maxwell_st%maxwell_rs_sign, dtmp)
        do idim = 1, maxwell_st%d%dim
          write(fname, '(2a)') 'free_maxwell_poynting_vector-', index2axis(idim)
          call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
        end do
      end if
      SAFE_DEALLOCATE_A(dtmp)
    end if

    POP_SUB(maxwell_output_poynting_vector)
  end subroutine maxwell_output_poynting_vector


  !----------------------------------------------------------
  subroutine maxwell_output_transverse_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)    !< have to set unit output correctly
    type(states_t),            intent(in)    :: maxwell_st
    type(grid_t),              intent(in)    :: maxwell_gr
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer :: idim, ierr
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:,:)

    PUSH_SUB(maxwell_output_transverse_rs_state)

    ! transverse component of the electric field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_TRANS_ELECTRIC_FIELD) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim))
      call maxwell_get_electric_field_state(maxwell_st%maxwell_rs_state_trans(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim), &
                                            dtmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim), &
                                            maxwell_st%maxwell_ep(1:maxwell_gr%mesh%np), maxwell_gr%mesh%np)
      do idim=1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_e_field_trans-', index2axis(idim)
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
      end do
      SAFE_DEALLOCATE_A(dtmp)
    end if

    ! transverse component of the magnetic field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_TRANS_MAGNETIC_FIELD) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim))
      call maxwell_get_electric_field_state(maxwell_st%maxwell_rs_state_trans(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim), &
                                            dtmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim), &
                                            maxwell_st%maxwell_ep(1:maxwell_gr%mesh%np), maxwell_gr%mesh%np)
      do idim=1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_b_field_trans-', index2axis(idim)
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
      end do
      SAFE_DEALLOCATE_A(dtmp)
    end if

    POP_SUB(maxwell_output_transverse_rs_state)
  end subroutine maxwell_output_transverse_rs_state


  !----------------------------------------------------------
  subroutine maxwell_output_longitudinal_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)    !< have to set unit output correctly
    type(states_t),            intent(in)    :: maxwell_st
    type(grid_t),              intent(in)    :: maxwell_gr
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer :: idim, ierr
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:,:)
    CMPLX, allocatable :: ztmp(:,:)

    PUSH_SUB(maxwell_output_longitudinal_rs_state)

    ! longitudinal component of the electric field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_LONG_ELECTRIC_FIELD) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(ztmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim))
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim))
      ztmp(1:maxwell_gr%mesh%np,:) = maxwell_st%maxwell_rs_state_long(1:maxwell_gr%mesh%np,:) 
      call maxwell_get_electric_field_state(ztmp, dtmp, maxwell_st%maxwell_ep(1:maxwell_gr%mesh%np), maxwell_gr%mesh%np)
      do idim=1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_e_field_long-', index2axis(idim)
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
      end do
      SAFE_DEALLOCATE_A(ztmp)
      SAFE_DEALLOCATE_A(dtmp)
    end if

    ! longitudinal component of the magnetic field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_LONG_MAGNETIC_FIELD) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(ztmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim))
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np,1:maxwell_st%d%dim))
      ztmp(1:maxwell_gr%mesh%np,:) = maxwell_st%maxwell_rs_state_long(1:maxwell_gr%mesh%np,:)
      call maxwell_get_magnetic_field_state(ztmp, maxwell_st%maxwell_rs_sign, dtmp, maxwell_st%maxwell_mu(1:maxwell_gr%mesh%np), &
                                            maxwell_gr%mesh%np)
      do idim=1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_b_field_long-', index2axis(idim)
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
      end do
      SAFE_DEALLOCATE_A(ztmp)
      SAFE_DEALLOCATE_A(dtmp)
    end if

    POP_SUB(maxwell_output_longitudinal_rs_state)
  end subroutine maxwell_output_longitudinal_rs_state


  !----------------------------------------------------------
  subroutine maxwell_output_divergence_rs_state(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)    !< have to set unit output correctly
    type(states_t),            intent(in)    :: maxwell_st
    type(grid_t),              intent(in)    :: maxwell_gr
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer :: ierr
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:,:)
    CMPLX, allocatable :: ztmp(:,:)

    PUSH_SUB(maxwell_output_divergence_rs_state)

    ! divergence of the electric field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_DIV_ELECTRIC_FIELD) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(ztmp(1:maxwell_gr%mesh%np_part,1))
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np_part,1))
      call maxwell_get_divergence_field(maxwell_gr, maxwell_st%maxwell_rs_state, ztmp(:,1), .false.)
      call maxwell_get_magnetic_field_state(ztmp, maxwell_st%maxwell_rs_sign, dtmp, &
                                            maxwell_st%maxwell_mu(1:maxwell_gr%mesh%np_part), maxwell_gr%mesh%np_part)
      call dio_function_output(maxwell_outp%how, dir, "maxwell_e_field_div", maxwell_gr%mesh, dtmp(:,1), fn_unit, ierr, geo=geo)
      SAFE_DEALLOCATE_A(ztmp)
      SAFE_DEALLOCATE_A(dtmp)
    end if

    ! divergence of the magnetic field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_DIV_MAGNETIC_FIELD) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(ztmp(1:maxwell_gr%mesh%np,1))
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np,1))
      call maxwell_get_divergence_field(maxwell_gr, maxwell_st%maxwell_rs_state, ztmp(:,1), .false.)
      call maxwell_get_magnetic_field_state(ztmp, maxwell_st%maxwell_rs_sign, dtmp, maxwell_st%maxwell_mu(1:maxwell_gr%mesh%np), &
                                            maxwell_gr%mesh%np)
      call dio_function_output(maxwell_outp%how, dir, "maxwell_b_field_div", maxwell_gr%mesh, dtmp(:,1), fn_unit, ierr, geo=geo)
      SAFE_DEALLOCATE_A(ztmp)
      SAFE_DEALLOCATE_A(dtmp)
    end if

    POP_SUB(maxwell_output_divergence_rs_state)
  end subroutine maxwell_output_divergence_rs_state


  !----------------------------------------------------------
  subroutine maxwell_output_vector_potential(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp)     !< have to set unit output correctly
    type(states_t),            intent(in)    :: maxwell_st
    type(grid_t),              intent(in)    :: maxwell_gr
    type(hamiltonian_t),       intent(in)    :: maxwell_hm
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer :: idim, ierr
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:)

    PUSH_SUB(maxwell_output_vector_potential)

    ! Maxwell vector potential
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_VECTOR_POTENTIAL) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np))
       do idim = 1, maxwell_gr%sb%dim
         write(fname, '(2a)') 'maxwell_vector_potential-', index2axis(idim)
         dtmp(1:maxwell_gr%mesh%np) = maxwell_hm%maxwell_vector_potential(1:maxwell_gr%mesh%np,idim)
         call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp, fn_unit, ierr, geo = geo)
       end do
      SAFE_DEALLOCATE_A(dtmp)
    end if

    POP_SUB(maxwell_output_vector_potential)
  end subroutine maxwell_output_vector_potential


  !------------------------------------------------------------
  subroutine maxwell_output_coupling_potentials(hm, gr, geo, dir, maxwell_outp)    !< have to set unit output correctly
    type(hamiltonian_t),       intent(in)    :: hm
    type(grid_t),              intent(in)    :: gr
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer :: ierr
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:)

    PUSH_SUB(maxwell_output_coupling_potentials)

    ! Maxwell electric dipole potential
    if (iand(hm%ep%mx_ma_coupling_order, OPTION__MAXWELLCOUPLINGORDER__ELECTRIC_DIPOLE_COUPLING) /= 0) then
      if (iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_ELECTRIC_DIPOLE_POTENTIAL) /= 0) then
        SAFE_ALLOCATE(dtmp(1:gr%mesh%np))
        fn_unit = unit_one
        dtmp(1:gr%mesh%np) = hm%ep%mx_ma_coupling_e_dipole_pot(1:gr%mesh%np)
        call dio_function_output(maxwell_outp%how, dir, "electric_dipole_potential", gr%mesh, dtmp, fn_unit, ierr, geo = geo)
        SAFE_DEALLOCATE_A(dtmp)
      end if
    end if

    ! Maxwell electric quadrupole potential
    if (iand(hm%ep%mx_ma_coupling_order, OPTION__MAXWELLCOUPLINGORDER__ELECTRIC_QUADRUPOLE_COUPLING) /= 0) then
      if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_ELECTRIC_QUADRUPOLE_POTENTIAL) /= 0) then
        SAFE_ALLOCATE(dtmp(1:gr%mesh%np))
        fn_unit = unit_one
        dtmp(1:gr%mesh%np) = hm%ep%mx_ma_coupling_e_quadrupole_pot(1:gr%mesh%np)
        call dio_function_output(maxwell_outp%how, dir, "electric_quadrupole_potential", gr%mesh, dtmp, fn_unit, ierr, geo = geo)
        SAFE_DEALLOCATE_A(dtmp)
      end if
    end if

    POP_SUB(maxwell_output_coupling_potentials)
  end subroutine maxwell_output_coupling_potentials


  !----------------------------------------------------------
  subroutine maxwell_output_current_density(maxwell_st, maxwell_gr, maxwell_hm, st, gr, hm, geo, dir, time, maxwell_outp)
    type(states_t),            intent(inout) :: maxwell_st
    type(grid_t),              intent(inout) :: maxwell_gr
    type(hamiltonian_t),       intent(in)    :: maxwell_hm
    type(states_t),            intent(inout) :: st
    type(grid_t),              intent(inout) :: gr
    type(hamiltonian_t),       intent(in)    :: hm
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    FLOAT,                     intent(in)    :: time
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer                     :: ierr, idim
    CMPLX, allocatable          :: maxwell_rs_density_ext(:,:)
    CMPLX, allocatable          :: ztmp_1(:,:), ztmp_2(:), ztmp_3(:,:)
    FLOAT, allocatable          :: dtmp_1(:,:)
    type(unit_t)                :: fn_unit

    PUSH_SUB(maxwell_output_current_density)

    ! Maxwell current density
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_CURRENT) /= 0) then
      fn_unit = (unit_one/units_out%time)/(units_out%length**2)      !< test both if its the same
      SAFE_ALLOCATE(ztmp_1(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
      SAFE_ALLOCATE(dtmp_1(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
      ztmp_1 = M_z0
      dtmp_1 = M_ZERO
      if (maxwell_hm%maxwell_operator == OPTION__MAXWELLHAMILTONIANOPERATOR__FARADAY_AMPERE_GAUSS) then
        SAFE_ALLOCATE(ztmp_2(1:maxwell_gr%mesh%np_part))
        ztmp_2 = m_Z0
      end if
      call maxwell_get_rs_density(maxwell_st, maxwell_gr, maxwell_hm, st, gr, hm, geo, hm%mx_ma_coupling_points, &
                                  hm%mx_ma_coupling_points_number, ztmp_1, ztmp_2, time, hm%current_prop_test)
      if (maxwell_hm%maxwell_operator == OPTION__MAXWELLHAMILTONIANOPERATOR__FARADAY_AMPERE_GAUSS) then
        SAFE_DEALLOCATE_A(ztmp_2)
      end if
      if (maxwell_hm%maxwell_current_density_ext_flag) then
        SAFE_ALLOCATE(ztmp_3(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
        call maxwell_get_rs_density_ext(maxwell_st, maxwell_gr%mesh, time, ztmp_3)
        ztmp_1 = ztmp_1 + ztmp_3
        SAFE_DEALLOCATE_A(ztmp_3)
      end if
      call maxwell_get_current_state(ztmp_1, dtmp_1, maxwell_st%maxwell_ep, maxwell_gr%mesh%np_part)
      dtmp_1 = - dtmp_1
      do idim = 1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_current-', index2axis(idim)
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp_1(:,idim), fn_unit, ierr, geo = geo)
      end do
      SAFE_DEALLOCATE_A(ztmp_1)
      SAFE_DEALLOCATE_A(dtmp_1)
    end if

    POP_SUB(maxwell_output_current_density)
  end subroutine maxwell_output_current_density


  !----------------------------------------------------------
  subroutine maxwell_output_external_current_density(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, time, maxwell_outp)
    type(states_t),            intent(inout) :: maxwell_st
    type(grid_t),              intent(inout) :: maxwell_gr
    type(hamiltonian_t),       intent(in)    :: maxwell_hm
    type(geometry_t),          intent(in)    :: geo
    character(len=*),          intent(in)    :: dir
    FLOAT,                     intent(in)    :: time
    type(output_t),            intent(in)    :: maxwell_outp

    character(len=MAX_PATH_LEN) :: fname
    integer                     :: ierr, idim
    CMPLX, allocatable          :: maxwell_rs_density_ext(:,:)
    CMPLX, allocatable          :: ztmp(:,:)
    FLOAT, allocatable          :: dtmp(:,:)
    type(unit_t)                :: fn_unit

    PUSH_SUB(maxwell_output_external_current_density)

    ! Maxwell current density
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_EXTERNAL_CURRENT) /= 0) then
      if (maxwell_hm%maxwell_current_density_ext_flag) then
        fn_unit = (unit_one/units_out%time)/(units_out%length**2)      !< test both if its the same
        SAFE_ALLOCATE(ztmp(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
        SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np_part,1:maxwell_st%d%dim))
        call maxwell_get_rs_density_ext(maxwell_st, maxwell_gr%mesh, time, ztmp)
        call maxwell_get_current_state(ztmp, dtmp, maxwell_st%maxwell_ep, maxwell_gr%mesh%np_part)
        dtmp = - dtmp
        do idim = 1, maxwell_st%d%dim
          write(fname, '(2a)') 'maxwell_external_current-', index2axis(idim)
          call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp(:,idim), fn_unit, ierr, geo = geo)
        end do
        SAFE_DEALLOCATE_A(ztmp)
        SAFE_DEALLOCATE_A(dtmp)
      end if
    end if

    POP_SUB(maxwell_output_external_current_density)
  end subroutine maxwell_output_external_current_density


  !----------------------------------------------------------
  subroutine maxwell_output_charge_density(maxwell_st, maxwell_gr, maxwell_hm, geo, dir, maxwell_outp, st, gr, hm)    !< have to set unit output correctly
    type(states_t),                intent(in)    :: maxwell_st
    type(grid_t),                  intent(in)    :: maxwell_gr
    type(hamiltonian_t),           intent(in)    :: maxwell_hm
    type(geometry_t),              intent(in)    :: geo
    character(len=*),              intent(in)    :: dir
    type(output_t),                intent(in)    :: maxwell_outp
    type(grid_t),        optional, intent(in)    :: gr
    type(states_t),      optional, intent(in)    :: st
    type(hamiltonian_t), optional, intent(in)    :: hm


    character(len=MAX_PATH_LEN) :: fname
    integer :: ierr
    type(unit_t) :: fn_unit
    FLOAT              :: density_diff_sum
    CMPLX, allocatable :: ztmp(:,:)
    FLOAT, allocatable :: dtmp_1(:,:), dtmp_2(:,:)

    PUSH_SUB(maxwell_output_charge_density)

    ! charge density calculated by the divergence of the electric field
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_CHARGE_DENSITY) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(ztmp(1:maxwell_gr%mesh%np_part,1))
      SAFE_ALLOCATE(dtmp_1(1:maxwell_gr%mesh%np_part,1))
      call maxwell_get_divergence_field(maxwell_gr, maxwell_st%maxwell_rs_state, ztmp(:,1), .true.)
      call maxwell_get_electric_field_state(ztmp, dtmp_1, maxwell_st%maxwell_ep, maxwell_gr%mesh%np_part)
      call dio_function_output(maxwell_outp%how, dir, "maxwell_charge_density", maxwell_gr%mesh, dtmp_1(:,1), &
                               fn_unit, ierr, geo=geo)
      SAFE_DEALLOCATE_A(ztmp)
      SAFE_DEALLOCATE_A(dtmp_1)
    end if

    if (present(st) .and. present(gr) .and. present(hm)) then
      if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_CHARGE_DENSITY_DIFF) /= 0) then
        fn_unit = unit_one
        SAFE_ALLOCATE(dtmp_1(1:maxwell_gr%mesh%np_part,1))
        SAFE_ALLOCATE(dtmp_2(1:maxwell_gr%mesh%np_part,1))
        call maxwell_get_charge_density_reference(gr, maxwell_gr, st, maxwell_st, dtmp_1(:,1), &
                                                        dtmp_2(:,1), density_diff_sum)
        call dio_function_output(maxwell_outp%how, dir, "maxwell_charge_density_diff", maxwell_gr%mesh, dtmp_2(:,1), &
                                fn_unit, ierr, geo=geo)
        SAFE_DEALLOCATE_A(dtmp_1)
        SAFE_DEALLOCATE_A(dtmp_2)
      end if
    end if

    POP_SUB(maxwell_output_charge_density)
  end subroutine maxwell_output_charge_density


  !----------------------------------------------------------
  subroutine maxwell_output_test_output(maxwell_st, maxwell_gr, geo, dir, maxwell_outp)
    type(states_t),         intent(inout) :: maxwell_st
    type(grid_t),           intent(inout) :: maxwell_gr
    type(geometry_t),       intent(in)    :: geo
    character(len=*),       intent(in)    :: dir
    type(output_t),         intent(in)    :: maxwell_outp

    integer :: idim, ierr
    character(len=MAX_PATH_LEN) :: fname
    type(unit_t) :: fn_unit
    FLOAT, allocatable :: dtmp(:)

    PUSH_SUB(maxwell_output_test_output)

    ! Test function output (real part)
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_TEST_OUTPUT) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np_part))
      do idim = 1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_test_output_real-', index2axis(idim)
        dtmp(1:maxwell_gr%mesh%np_part) = real(maxwell_st%maxwell_test_output(1:maxwell_gr%mesh%np_part,idim))
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp, fn_unit, ierr, geo = geo)
      end do
      SAFE_DEALLOCATE_A(dtmp)
    end if

    ! Test function output (imaginary part)
    if(iand(maxwell_outp%what, OPTION__MAXWELLOUTPUT__MAXWELL_TEST_OUTPUT) /= 0) then
      fn_unit = unit_one
      SAFE_ALLOCATE(dtmp(1:maxwell_gr%mesh%np_part))
      do idim = 1, maxwell_st%d%dim
        write(fname, '(2a)') 'maxwell_test_output_imag-', index2axis(idim)
        dtmp(1:maxwell_gr%mesh%np_part) = aimag(maxwell_st%maxwell_test_output(1:maxwell_gr%mesh%np_part,idim))
        call dio_function_output(maxwell_outp%how, dir, fname, maxwell_gr%mesh, dtmp, fn_unit, ierr, geo = geo)
      end do
      SAFE_DEALLOCATE_A(dtmp)
    end if

    POP_SUB(maxwell_output_test_output)
  end subroutine maxwell_output_test_output  



!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
